# Anagram checker frontend

## Introduction

This is a simple prototype website that I used for learning svelte.
It offers a simple website that can talk to the related backend service at <https://gitlab.com/iron9/anagram-checker>.

## Get started

1. You need node v18.16.0. One way is to install nvm and then run `nvm use` in this directory (only on Unix).
2. Run `npm install` to install all dependencies.
3. You need a running backend that is accessible via URL.
4. Run `VITE_API_URL=<API_URL> npm run dev` in order to start a dev server. Replace `<API_URL>` by the actual URL. Alternatively, put the API_URL in the `.env` file. (Use the same name.) In that case, you can run the server with `npm run dev`.
5. The URL to reach the website will be printed to the terminal.
